package ru.nsu.g.avoronkov.helloworld;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import ru.nsu.g.avoronkov.helloworld.Hello;

public class HelloTest {
	@Test
	public void testGreet() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String content = "";
		try(PrintStream ps = new PrintStream(baos, true, "utf-8")) {
			Hello hello = new Hello();
			hello.greet(ps, "anybody");
			content = new String(baos.toByteArray(), StandardCharsets.UTF_8);
		}
		catch (Exception e) {
			fail(e.toString());
		}
		assertEquals(content, "Hello anybody!\n");
	}
}
