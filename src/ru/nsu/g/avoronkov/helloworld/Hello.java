package ru.nsu.g.avoronkov.helloworld;

import java.io.PrintStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Properties;

public class Hello {
	public Hello() throws IOException {
		try (InputStream in = this.getClass().getResourceAsStream("Hello.properties")) {
			this.properties = new Properties();
			properties.load(in);

		}
	}

	public void greet(PrintStream out, String someone) {
		String greeting = this.properties.getProperty("greeting");
		out.printf("%s %s!\n", greeting, someone);
	}

	private Properties properties;
}
