package ru.nsu.g.avoronkov.helloworld;

public class Main {
	public static void main(String[] args) {
		try {
			Hello hello = new Hello();
			hello.greet(System.out, "world");
		}
		catch (Exception e) {
			System.err.printf("helloworld failed: %s", e);
			System.exit(1);
		}
	}
}
